/* -*- mode: C++ -*- */
//////////////////////////////////////////////////////////////////////////////
// FILE   : test.tol
// PURPOSE: Package MatQuery test
//////////////////////////////////////////////////////////////////////////////

Text email_ = "vdebuen@tol-project.org"; 
Text link_ = ""; 
Text summary_ = "Checking MatAlg::@Cholesky";

//Write here the initial test TOL code if needed
Real numErr0 = Copy(NError);
Real numWar0 = Copy(NWarning);

#Require MatAlg;

//Random seed setting
Real PutRandomSeed(289217511);
Real rndSeed = GetRandomSeed(0);
WriteLn("Current Random Seed = "<<rndSeed);


Real n= IntRand(1,100);
Real m= IntRand(3,20)*n;
Real k= IntRand(1,Max(1,n-1));
Real dense = IntRand(0,1);

VMatrix X = If(dense, Rand(m,n,-3,3),
                      Rand(m,n,-3,3,"Cholmod.R.Sparse",Min(n,3)*m))+
            Eye(m,n)*0.001;

VMatrix M = If(dense, Rand(n,k,-3,3),
                      Rand(n,k,-3,3,"Cholmod.R.Sparse",0.05*n*k));
VMatrix Mt = Tra(M);

VMatrix R = 
{
  VMatrix aux = MtMSqr(X);
  If(dense,Convert(aux,"Blas.R.Dense"),aux) 
};

VMatrix S = 
{
  VMatrix aux = Mat2VMat(SVDInverse(VMat2Mat(R)));
  If(dense,Convert(aux,"Blas.R.Dense"),aux) 
};

VMatrix R.S = R*S;

MatAlg::@Cholesky chol.X = MatAlg::@Cholesky::Build("X",X);
MatAlg::@Cholesky chol.S = MatAlg::@Cholesky::Build("S",S);

/* */

VMatrix K = chol.X::_.Ke;
VMatrix Kit = Mat2VMat(SVDInverse(VMat2Mat(K)),True);
VMatrix L = chol.S::_.Le;
VMatrix LD0 = SubDiag(L,0);

VMatrix KKt = MMtSqr(K);
VMatrix LLt = MMtSqr(L);

//VMatrix KD0 = SubDiag(K,0);
//VMatrix LD0 = SubDiag(L,0);

Real log_det_S.X = chol.X::log_det_S(?);
Real log_det_S.S = chol.S::log_det_S(?);


/*
VMatrix L.M.X = chol.X::L_prod_M(M);
VMatrix Kt.L.M.X = chol.X::Kt_prod_M(L.M.X);

VMatrix Lt.M.X = chol.X::Lt_prod_M(M);
VMatrix K.Lt.M.X = chol.X::K_prod_M(Lt.M.X);

VMatrix K.M.X = chol.X::K_prod_M(M);
VMatrix Lt.K.M.X = chol.X::Lt_prod_M(K.M.X);

VMatrix Kt.M.X = chol.X::Kt_prod_M(M);
VMatrix L.Kt.M.X = chol.X::L_prod_M(Kt.M.X);

VMatrix Mt.L.X = chol.X::M_prod_L(Mt);
VMatrix Mt.L.Kt.X = chol.X::M_prod_Kt(Mt.L.X);

VMatrix Mt.Lt.X = chol.X::M_prod_Lt(Mt);
VMatrix Mt.Lt.K.X = chol.X::M_prod_K(Mt.Lt.X);

VMatrix Mt.K.X = chol.X::M_prod_K(Mt);
VMatrix Mt.K.Lt.X = chol.X::M_prod_Lt(Mt.K.X);

VMatrix Mt.Kt.X = chol.X::M_prod_Kt(Mt);
VMatrix Mt.Kt.L.X = chol.X::M_prod_L(Mt.Kt.X);

VMatrix L.M.S = chol.S::L_prod_M(M);
VMatrix Kt.L.M.S = chol.S::Kt_prod_M(L.M.S);

VMatrix Lt.M.S = chol.S::Lt_prod_M(M);
VMatrix K.Lt.M.S = chol.S::K_prod_M(Lt.M.S);

VMatrix K.M.S = chol.S::K_prod_M(M);
VMatrix Lt.K.M.S = chol.S::Lt_prod_M(K.M.S);

VMatrix Kt.M.S = chol.S::Kt_prod_M(M);
VMatrix L.Kt.M.S = chol.S::L_prod_M(Kt.M.S);

VMatrix Mt.L.S = chol.S::M_prod_L(Mt);
VMatrix Mt.L.Kt.S = chol.S::M_prod_Kt(Mt.L.S);

VMatrix Mt.Lt.S = chol.S::M_prod_Lt(Mt);
VMatrix Mt.Lt.K.S = chol.S::M_prod_K(Mt.Lt.S);

VMatrix Mt.K.S = chol.S::M_prod_K(Mt);
VMatrix Mt.K.Lt.S = chol.S::M_prod_Lt(Mt.K.S);

VMatrix Mt.Kt.S = chol.S::M_prod_Kt(Mt);
VMatrix Mt.Kt.L.S = chol.S::M_prod_L(Mt.Kt.S);

VMatrix Mt.R.M   = Mt*R*M;
VMatrix Mt.R.M.X = chol.X::Mt_prod_R_prod_M(M);
VMatrix Mt.R.M.S = chol.S::Mt_prod_R_prod_M(M);
VMatrix Mt.S.M   = Mt*S*M;
VMatrix Mt.S.M.X = chol.X::Mt_prod_S_prod_M(M);
VMatrix Mt.S.M.S = chol.S::Mt_prod_S_prod_M(M);


Set ok.all = [[
Real ok.R.S       = 1-VMatMax(Abs(R.S-Eye(n)));
Real ok.KKt       = 1-VMatMax(Abs(KKt-R));
Real ok.LLt       = 1-VMatMax(Abs(LLt-S));
Real ok.K.Lt.M.X  = 1-VMatMax(Abs(K.Lt.M.X-M));
Real ok.Lt.K.M.X  = 1-VMatMax(Abs(Lt.K.M.X-M));
Real ok.L.Kt.M.X  = 1-VMatMax(Abs(L.Kt.M.X-M));
Real ok.Mt.L.Kt.X = 1-VMatMax(Abs(Mt.L.Kt.X-Mt));
Real ok.Mt.Lt.K.X = 1-VMatMax(Abs(Mt.Lt.K.X-Mt));
Real ok.Mt.K.Lt.X = 1-VMatMax(Abs(Mt.L.Kt.X-Mt));
Real ok.Mt.Kt.L.X = 1-VMatMax(Abs(Mt.Kt.L.X-Mt));
Real ok.K.Lt.M.S  = 1-VMatMax(Abs(K.Lt.M.S-M));
Real ok.Lt.K.M.S  = 1-VMatMax(Abs(Lt.K.M.S-M));
Real ok.L.Kt.M.S  = 1-VMatMax(Abs(L.Kt.M.S-M));
Real ok.Mt.L.Kt.S = 1-VMatMax(Abs(Mt.L.Kt.S-Mt));
Real ok.Mt.Lt.K.S = 1-VMatMax(Abs(Mt.Lt.K.S-Mt));
Real ok.Mt.K.Lt.S = 1-VMatMax(Abs(Mt.L.Kt.S-Mt));
Real ok.Mt.Kt.L.S = 1-VMatMax(Abs(Mt.Kt.L.S-Mt));
Real ok.Kt.L.M.S  = 1-VMatMax(Abs(Kt.L.M.S-M));
Real ok.Mt.R.M.X  = 1-VMatMax(Abs(Mt.R.M.X-Mt.R.M));
Real ok.Mt.R.M.S  = 1-VMatMax(Abs(Mt.R.M.S-Mt.R.M));
Real ok.Mt.S.M.X  = 1-VMatMax(Abs(Mt.S.M.X-Mt.S.M));
Real ok.Mt.S.M.S  = 1-VMatMax(Abs(Mt.S.M.S-Mt.S.M))
]];

Real ok.min = SetMin(ok.all);


Real numErr1 = Copy(NError);
Real numWar1 = Copy(NWarning);

Set partialResults_ = [[numErr0, numErr1, n, m, k, dense]]<<ok.all;

//This is a messure of the success of the test 
Real quality_ = And(numErr1 == numErr0)*ok.min;

//Return the results 
Set resultStr_ = @strTestStatus(summary_, link_, quality_,
                  "Partial results = "<<partialResults_,
                  "NO DBServerType", "NO DBServerHost", "NO DBAlias",
                  email_);
WriteLn(""<<resultStr_);
resultStr_;

/* */

